function drawCircles () {
    let body = document.getElementsByTagName("body");
    let i = 0;
    while (i < 100){
       let d = document.createElement('div');
        d.className = 'circle';
        d.style.backgroundColor = generateColor();
            document.body.appendChild(d);
        i++;

    }
}
function generateColor () {
    let color = '#';
    let col;
    let k = 0;
    while (k<3){ 
        col = dec2hex(Math.round(Math.random()*256));
        color = color + col;
        k++;
    }
    return color;
}
function dec2hex(i) {
    return (i+0x10000).toString(16).substr(-2).toUpperCase();
 }