/*
За каждую операцию будет отвечать отдельная функция, т.е. для сложения - add(a,b), для умножения - multiple(a,b) и т.д. Каждая из них принимает в аргументы только два числа и возвращает результат операции над двумя числами Если число не передано в функцию аргументом - ПО УМОЛЧАНИЮ присваивать этому аргументу 0.


Основная функция calculate()
Принимает ТРИ АРГУМЕНТА:
1 - число
2 - число
3 - функция которую нужно выполнить для двух этих чисел. Таким образом получается что основная функция калькулятор будет вызывать переданную ей аргументом функцию для двух чисел, которые передаются остальными двумя аргументами. При делении на 0 выводить ошибку. Функия калькулятор доджна принять на вход 3 аругмента, Если аргументов больше или меньше выводить ошибку.
**/



function calculator (first,second,callback) {
    return (callback(first,second));
}

const add =  (a,b) => {
    return a+b;
}

const mult =  (a,b) => {
    return a*b;
}

const sub =  (a,b) => {
    return a-b;
}

const div =  (a,b) => {
    return a/b;
}

function show(msg){
    document.write(msg);
}


function setDate (){
    let operand1 = parseInt(prompt('input first number'));
    let operand2 = parseInt(prompt('input second number'));
    let sign = prompt('Введите знак');
    switch (sign){
    case   '+' :   show(calculator (operand1,operand2,add)); break;
    case   '-' :   show(calculator (operand1,operand2,sub)); break;
    case   '*' :   show(calculator (operand1,operand2,mult)); break;
    case   '/' :   show(calculator (operand1,operand2,div)); break;
    }
}

setDate();