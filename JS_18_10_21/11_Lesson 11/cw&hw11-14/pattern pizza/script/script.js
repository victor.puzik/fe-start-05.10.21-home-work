var miniPrice = 50;
var midPrice = 100;
var maxPrice = 150;
var cetchupPrice = 10;
var bbqPrice = 15;
var ricottaPrice = 20;
var price = [miniPrice,midPrice,maxPrice];
var mozzarellaPrice = 50;
var cheesePrice = 68;
var fetaPrice = 74;

class Pizza {
    constructor (size, smallPrice, midlPrice, bigPrice, 
        cetchupPrice, cetchupQty,
        bbqPrice, bbqQty,
        ricottaPrice, ricotteQty,
        cheesePrice, cheeseQty,
        mozzarellaPrice, mozzarellaQty,
        fetaPrice, fetaQty){
        this.size = size = 'big';
        this.smallPrice = smallPrice;
        this.midlPrice = midlPrice;
        this.bigPrice = bigPrice;
        this.cetchupPrice = cetchupPrice;
        this.cetchupQty = cetchupQty = 0;
        this.bbqPrice = bbqPrice;
        this.bbqQty = bbqQty = 0;
        this.ricottaPrice = ricottaPrice;
        this.ricotteQty = ricotteQty = 0;
        this.mozzarellaPrice = mozzarellaPrice;
        this.mozzarellaQty = mozzarellaQty = 0;
        this.fetaPrice = fetaPrice;
        this.fetaQty = fetaQty = 0;
        this.cheesePrice = cheesePrice;
        this.cheeseQty = cheeseQty = 0;
    }
    allPrice (){
        let res = 0;
        switch (this.size)
        {
            case 'small' :
                res += this.smallPrice;
                break;
            case 'mid' :
                res += this.midlPrice;
                break;
            case 'big' :
                res += this.bigPrice;
                break;
        }
        res += this.bbqPrice*this.bbqQty;
        res += this.cetchupPrice*this.cetchupQty;
        res += this.ricottaPrice*this.ricotteQty;
        res += this.cheesePrice*this.cheeseQty;
        res += this.mozzarellaPrice*this.mozzarellaQty;
        res += this.fetaPrice*this.fetaQty;
        return res;
    }
    priceShow (){
        document.querySelector('.price').innerHTML = `<p>Ціна: ${this.allPrice()} грн.</p>`;
    }
    addIngredient (ingredient){
        switch(ingredient)
        {
            case 'cetchup':
                this.cetchupQty++;
                break;
            case 'bbq':
                this.bbqQty++;
                break;
            case 'ricotta':
                this.ricotteQty++;
                break;
            case 'mozzarella':
                this.mozzarellaQty++;
                break;
            case 'feta':
                this.fetaQty++;
                break;
            case 'cheese':
                this.cheeseQty++;
                break;
        }
    }
    checkQty (ingredient) {
        let res = 0;
        switch(ingredient)
        {
            case 'cetchup':
                res = this.cetchupQty;
                break;
            case 'bbq':
                res = this.bbqQty;
                break;
            case 'ricotta':
                res = this.ricotteQty;
                break;
            case 'mozzarella':
                res = this.mozzarellaQty;
                break;
            case 'feta':
                res = this.fetaQty;
                break;
            case 'cheese':
                res = this.cheeseQty;
                break;
        }
        return res;
    }
}

const pizza = new(Pizza);

pizza.smallPrice = miniPrice;
pizza.midlPrice = midPrice;
pizza.bigPrice = maxPrice;
pizza.ricottaPrice = ricottaPrice;
pizza.bbqPrice = bbqPrice;
pizza.cetchupPrice = cetchupPrice;
pizza.cheesePrice = cheesePrice;
pizza.mozzarellaPrice = mozzarellaPrice;
pizza.fetaPrice = fetaPrice;


window.addEventListener('DOMContentLoaded', ()=>{
    // показываем выпадающее меню
    const bm = document.querySelector(".bm");
    bm.addEventListener('click', (e)=>{
        document.querySelector('.nv ul').setAttribute('position', 'absolute');
        document.querySelector('.nv ul').setAttribute('left', e.pageX-60+"px");
        document.querySelector('.nv ul').classList.toggle('active');
        document.getElementsByClassName('active')[0].style.top = e.clientY+5+"px"
    });
    const list = document.querySelector('.nv ul');
    list.addEventListener('mouseout', (r)=>{
        if(document.querySelector('.nv ul').classList.contains('active')){
            document.querySelector('.nv ul').setAttribute('display', 'none');
        }
    })

//Select size of pizza
    const size = document.querySelectorAll('.radio');
    size[0].addEventListener ('click', ()=>{
        pizza.size = 'small';
        pizza.priceShow();
    });
    size[1].addEventListener ('click', ()=>{
        pizza.size = 'mid';
        pizza.priceShow();
    });
    size[2].addEventListener ('click', ()=>{
        pizza.size = 'big';
        pizza.priceShow();
    });

    // dragstart - вызывается в самом начале переноса перетаскиваемого элемента.
    // dragend - вызывается в конце события перетаскивания - как успешного, так и отмененного.
    // dragenter - происходит в момент когда перетаскиваемый объект попадает в область целевого элемента.
    // dragleave - происходит когда перетаскиваемый элемент покидает область целевого элемента.
    // dragover - происходит когда перетаскиваемый элемент находиться над целевым элементом.
    // drop - вызывается, когда событие перетаскивания завершается отпусканием элемента над целевым элементом.

   //select of sauces
   //Кетчуп
    const cetchupDrug = document.getElementById("sauceClassic");
    cetchupDrug.addEventListener('dragstart', function(evt){
                evt.dataTransfer.effectAllowed = "move";
                evt.dataTransfer.setData("Text", this.id);
                evt.dataTransfer.setData("pizza", "cetchup");
        }, false);
    //BBQ sauce
    const bbqDrug = document.getElementById("sauceBBQ");
    bbqDrug.addEventListener('dragstart', function(evt){
                evt.dataTransfer.effectAllowed = "move";
                evt.dataTransfer.setData("Text", this.id);
                evt.dataTransfer.setData("pizza", "bbq");
        }, false);
    const riccotaDrug = document.getElementById("sauceRikotta");
    riccotaDrug.addEventListener('dragstart', function(evt){
                evt.dataTransfer.effectAllowed = "move";
                evt.dataTransfer.setData("Text", this.id);
                evt.dataTransfer.setData("pizza", "ricotta");
        }, false);
    const mozzarellaDrug = document.getElementById("moc3");
    mozzarellaDrug.addEventListener('dragstart', function(evt){
                evt.dataTransfer.effectAllowed = "move";
                evt.dataTransfer.setData("Text", this.id);
                evt.dataTransfer.setData("pizza", "mozzarella");
        }, false);
    const fetaDrug = document.getElementById("moc2");
    fetaDrug.addEventListener('dragstart', function(evt){
                evt.dataTransfer.effectAllowed = "move";
                evt.dataTransfer.setData("Text", this.id);
                evt.dataTransfer.setData("pizza", "feta");
        }, false);
    const cheeseDrug = document.getElementById("moc1");
    cheeseDrug.addEventListener('dragstart', function(evt){
                evt.dataTransfer.effectAllowed = "move";
                evt.dataTransfer.setData("Text", this.id);
                evt.dataTransfer.setData("pizza", "cheese");
        }, false);
    
    const cetchupDrop = document.getElementById("cakediv");

    cetchupDrop.addEventListener("dragover", function (evt) {
        // отменяем стандартное обработчик события dragover.
        // реализация данного обработчика по умолчанию не позволяет перетаскивать данные на целевой элемент, так как большая
        // часть веб страницы не может принимать данные.
        // Для того что бы элемент мог принять перетаскиваемые данные необходимо отменить стандартный обработчик.
        if (evt.preventDefault) evt.preventDefault();
        return false;
    }, false);
    // перетаскиваемый элемент отпущен над целевым элементом
    cetchupDrop.addEventListener("drop", function (evt) {
        // прекращаем дальнейшее распространение события по дереву DOM и отменяем возможный стандартный обработчик установленный браузером.
        if (evt.preventDefault) evt.preventDefault();
        if (evt.stopPropagation) evt.stopPropagation();
        var id = evt.dataTransfer.getData("Text"); // получаем информации которая передавалась через операцию drag & drop 
        var elem = document.getElementById(id);
        console.log(evt.dataTransfer.getData("pizza"));
        console.log(elem.src);
        if (!pizza.checkQty(evt.dataTransfer.getData("pizza"))){
            const addImg = document.createElement("img")
            addImg.setAttribute('src', elem.src)
            document.querySelector('.table').appendChild(addImg);
        }
        evt.dataTransfer.effectAllowed = "none";
        pizza.addIngredient(evt.dataTransfer.getData("pizza"));
        pizza.priceShow();
   }, false);





/*
    const minSize = document.getElementById('minSize');
    minSize.addEventListener('click', ()=> {
        document.querySelector('.price').innerHTML = `<p>Ціна: ${miniPrice} грн.</p>`;
    });

    const midSize = document.getElementById('midSize');
    midSize.addEventListener('click', ()=> {
        document.querySelector('.price').innerHTML = `<p>Ціна: ${midPrice} грн.</p>`;
    });

    const bigSize = document.getElementById('bigSize');
    bigSize.addEventListener('click', ()=> {
        document.querySelector('.price').innerHTML = `<p>Ціна: ${maxPrice} грн.</p>`;
    });
    */
    /*
    const size = document.getElementById('pizza');
    size.addEventListener('click', (i)=>{
         var rr= [...document.querySelectorAll('#pizza .radio input')];
         debugger;
         var i = 0;
         while (rr.length > i){
            if (rr[i].checked){
                document.querySelector('.price').innerHTML = `<p>Ціна: ${price[i]} грн.</p>`;
                console.dir(document.querySelector('.price').innerHTML);
                break;
            } 
            i++;
        }
    })
    */
    // отправка заказа
    const btnSubmit = document.getElementById('btnSubmit');
    btnSubmit.addEventListener('click', (e)=>{
        const clientName = document.getElementById('name').value;
        const clientMail = document.getElementById('number').value;
        const clientNumber = document.getElementById('mail').value;
        var valid = false;
        const expName = /^[\w]{2,}$/i;
        const expNumber = /^\+[\d]{12,12}$/;
        const expMail = /^[\w]{1}[\w-\.]*@[\w-]+\.[a-z]{2,4}$/i;
        //проверяем длину
        if ( clientName.length&&clientNumber.length&&clientMail.length){
            //проверяем имя
            if (!expName.test(clientName))
            {
                //Неверное имя
                valid = false;
                document.getElementById('name').classList.toggle('nonvalidate');
            }
            else if (!expNumber.test(clientNumber)){
                //Неверный номер
                valid = false;
                document.getElementById('number').classList.toggle('nonvalidate');
            }
            else if (!expMail.test(clientMail)){
                //Неверная почта
                valid = false;
                document.getElementById('mail').classList.toggle('nonvalidate');             
            } else {valid = true;}
        }
        // send data
        if (valid){
            !valid;
        }
        //console.log(document.getElementById('name').value);
    })



})