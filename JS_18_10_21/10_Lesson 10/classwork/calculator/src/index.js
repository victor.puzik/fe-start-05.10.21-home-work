/* * В папке calculator дана верстка макета калькулятора. Необходимо сделать этот калькулятор рабочим.
* При клике на клавиши с цифрами - набор введенных цифр должен быть показан на табло калькулятора.
* При клике на знаки операторов (`*`, `/`, `+`, `-`) на табло ничего не происходит - программа ждет введения второго числа для выполнения операции.
* Если пользователь ввел одно число, выбрал оператор, и ввел второе число, то при нажатии как кнопки `=`, так и любого из операторов, в табло должен появиться результат выполенния предыдущего выражения.
* При нажатии клавиш `M+` или `M-` в левой части табло необходимо показать маленькую букву `m` - это значит, что в памяти хранится число. Нажатие на `MRC` покажет число из памяти на экране. Повторное нажатие `MRC` должно очищать память.
 */

window.addEventListener("DOMContentLoaded", () => {
    const btns = document.querySelector(".keys"),
        display = document.querySelector('.display input'),
        resBtn =  document.querySelector('.button.orange');
    class Calculator  {
        constructor (firstNumber,secondNumber,sign,result,memory){
        this.firstNumber = firstNumber = "";
        this.secondNumber = secondNumber = "";
        this.sign = sign = "";
        this.result = result = "";
        this.memory = memory = "";
        }
        calculate () {
            switch (this.sign) {
                case "+":
                    this.result = parseInt(this.firstNumber) + parseInt(this.secondNumber);
                    break;
                case "-":
                    this.result = parseInt(this.firstNumber) - parseInt(this.secondNumber);
                    break;
                case "/":
                    this.result = parseFloat(this.firstNumber) / parseFloat(this.secondNumber);
                    break;
                case "*":
                    this.result = parseInt(this.firstNumber) * parseInt(this.secondNumber);
                    break;
                default:
                    break;
            }
            this.firstNumber = "";
            this.secondNumber = "";
            this.sign = "";            
        }
    }
    let calculator = new Calculator;

    btns.addEventListener("click", (e) => {
        console.log(e.target.value);
        validateCalc (e.target.value,show);
        console.log(resBtn);
    })

    function validateCalc(value, callback) {
        if (calculator.sign == "" && calculator.firstNumber.length <= 10 && value !== undefined && value.search(/\d/) !== -1) {
            calculator.firstNumber += value;
            callback(calculator.firstNumber);
        }
        if (calculator.secondNumber.length == 0 && value !== undefined && sign (value))
        {
            calculator.sign = value;
        }

        if (calculator.sign != "" && calculator.secondNumber.length <= 10 && value !== undefined && value.search(/\d/) !== -1) {
            calculator.secondNumber += value;
            resBtn.removeAttribute('disabled');
            callback(calculator.secondNumber);
        }
        if (value === "=")
        {
            resBtn.setAttribute('disabled', 'disabled');
            calculator.calculate();
            callback(calculator.result);
        }
    }

    function show (data){
        display.value = data
    }

    function sign (symbol){
        if (symbol != "-" && symbol != "+" && symbol != "/" && symbol != "*"){
            return 0;
        }
        else {return 1;}
        
    }


})